USE blog_db;

-- insert users

INSERT INTO users (email, password, datetime_created)
    VALUES(
        "johnsmith@gmail.com",
        "passwordA", 
        "2021-1-1 01:00:00"
    );

INSERT INTO users (email, password, datetime_created)
    VALUES(
        "juandelacruz@gmail.com",
        "passwordB", 
        "2021-1-1 02:00:00"
    );

INSERT INTO users (email, password, datetime_created)
    VALUES(
        "janesmith@gmail.com",
        "passwordC", 
        "2021-1-1 03:00:00"
    );

INSERT INTO users (email, password, datetime_created)
    VALUES(
        "mariadelacruz@gmail.com",
        "passwordD", 
        "2021-1-1 04:00:00"
    );

INSERT INTO users (email, password, datetime_created)
    VALUES(
        "johndoe@gmail.com",
        "passwordE", 
        "2021-1-1 05:00:00"
    );

-- insert posts

INSERT INTO posts (author_id, title, content, datetime_posted)
    VALUES(
        1,
        "First Code",
        "Hello World!", 
        "2021-1-2 01:00:00"
    );

INSERT INTO posts (author_id, title, content, datetime_posted)
    VALUES(
        1,
        "Second Code",
        "Hello Earth!", 
        "2021-1-2 02:00:00"
    );

INSERT INTO posts (author_id, title, content, datetime_posted)
    VALUES(
        2,
        "Third Code",
        "Welcome to Mars!", 
        "2021-1-2 03:00:00"
    );

INSERT INTO posts (author_id, title, content, datetime_posted)
    VALUES(
        4,
        "Fourth Code",
        "Bye bye solar system!", 
        "2021-1-2 04:00:00"
    );

-- get all the post with an Author ID of 1:

SELECT * FROM posts WHERE author_id = 1;

-- get all the user's email and datetime of creation:

SELECT email, datetime_created FROM users;

-- update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID:

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- delete the user with an email of "johndoe@gmail.com":

DELETE FROM users WHERE email = "johndoe@gmail.com";